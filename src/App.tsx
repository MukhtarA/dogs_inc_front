import React, {useCallback, useEffect, useRef, useState} from 'react';
import {useSelector} from "react-redux";
import * as _ from "lodash";
import {Box, Button, Modal} from "@mui/material";

import {useAppDispatch} from "./app/hooks";
import {
    getAllBreeds,
    getPhotoByBreed,
    selectorAllBreeds, selectorAllBreedsError,
    selectorAllBreedsStatus,
    selectorBreedPhotoData,
    selectorBreedPhotoStatus
} from "./app/slice";
import {Header} from "./features/header";
import {Grid, ContentWrapper} from "./style";
import {BreedCard} from "./features/breed-card";

function App() {
    const dispatch = useAppDispatch()
    const dogs = useSelector(selectorAllBreeds)
    const dogsStatus = useSelector(selectorAllBreedsStatus)
    const dogsError = useSelector(selectorAllBreedsError)
    const images = useSelector(selectorBreedPhotoData)
    const imagesStatus = useSelector(selectorBreedPhotoStatus)
    const [selectedDog, setSelectedDog] = useState('')
    const [modalOpen, setModalOpen] = useState(false)

    const style = {
        position: 'absolute' as 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 400,
        boxShadow: 24,
        pt: 2,
        px: 4,
        pb: 3,
    };

    const handleClick = useCallback((e: any) => {
        setSelectedDog(e)
    }, [])

    const handleUpdatePhoto = useCallback((value: any) => {
        dispatch(getPhotoByBreed({selectedDog, value}))
    }, [dispatch, selectedDog])

    const handleCancel = useCallback((value: any) => {
        value.abort()
    }, [])

    useEffect(() => {
        dispatch(getAllBreeds())
    }, [dispatch])

    useEffect(() => {
        if (dogsStatus === 'failed'){
            setModalOpen(true)
        }
    }, [dogsStatus])

    return (
        <div>
            <Header/>
            <ContentWrapper style={{marginTop: '9rem'}}>
                <Grid style={{marginBottom: '2rem'}}>
                    {_.map(dogs, (item, idx) => {
                            return (
                                <BreedCard
                                    handleCardClick={handleClick}
                                    key={item?.breed}
                                    breed={item?.breed}
                                    subBreed={item?.subBreed}
                                    isActive={item?.breed === selectedDog}
                                    isLoading={imagesStatus === 'loading'}
                                    handleUpdate={handleUpdatePhoto}
                                    handleCancel={handleCancel}
                                    images={images}
                                />

                            )
                        }
                    )}
                </Grid>
            </ContentWrapper>
            <Modal
                hideBackdrop
                open={modalOpen}
                onClose={() => setModalOpen(false)}
            >
                <Box  sx={{ ...style, width: 200 }}>
                    <h2>Service error</h2>
                    <p>
                        Whoops, Something Went Wrong.
                    </p>
                    <Button onClick={() => setModalOpen(false)}>Close Modal</Button>
                </Box>
            </Modal>
        </div>
    );
}

export default App;
