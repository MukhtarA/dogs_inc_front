import {createAsyncThunk, createSlice, PayloadAction} from '@reduxjs/toolkit';
import axios from "axios";
import * as _ from "lodash";

import {RootState} from './store';


interface BaseList {
    data: any;
    status: 'idle' | 'loading' | 'failed' | 'succeeded';
    error: any
}

export interface CounterState {
    allBreedsList: BaseList;
    getPhotoByBreed: BaseList;
    getPhotoBySubBreed: BaseList;
    selectedBreed: string;
}

const initialState: CounterState = {
    allBreedsList: {
        data: null,
        status: 'idle',
        error: null
    },
    getPhotoByBreed: {
        data: null,
        status: 'idle',
        error: null
    },
    getPhotoBySubBreed: {
        data: null,
        status: 'idle',
        error: null
    },
    selectedBreed: ''
};

export const getAllBreeds = createAsyncThunk('getAllBreeds', async () => {
    const response = await axios.get('https://dog.ceo/api/breeds/list/all');

    return response.data.message;
})

export const getPhotoByBreed = createAsyncThunk('getPhotoByBreed', async (data: any) => {
    const {selectedDog, value} = data;
    const response = await axios.get(
        `https://dog.ceo/api/breed/${selectedDog}/images/random/3`,
        {signal: value.signal, timeout: 10000});

    return response.data.message;
})

export const getPhotoBySubBreed = createAsyncThunk('getPhotoBySubBreed', async (data: any) => {
    const {breed, subBreed} = data
    const response = await axios.get(`https://dog.ceo/api/breed/${breed}/${subBreed}/images`);

    return response.data.message;
})

export const slice = createSlice({
    name: 'mainSlice',
    initialState,
    reducers: {
        setSelectedBreed: (state, action: PayloadAction<string>) => {
            state.selectedBreed = action.payload
        }
    },
    extraReducers: (builder) => {
        builder
            .addCase(getAllBreeds.pending, (state, action) => {
                state.allBreedsList.status = 'loading';
            })
            .addCase(getAllBreeds.fulfilled, (state, action) => {
                state.allBreedsList.status = 'succeeded';
                state.allBreedsList.data = action.payload;
            })
            .addCase(getAllBreeds.rejected, (state, action) => {
                state.allBreedsList.status = 'failed';
                state.allBreedsList.error = action.payload
            })
            .addCase(getPhotoByBreed.pending, (state, action) => {
                state.getPhotoByBreed.status = 'loading';
            })
            .addCase(getPhotoByBreed.fulfilled, (state, action) => {
                state.getPhotoByBreed.status = 'succeeded';
                state.getPhotoByBreed.data = action.payload;
            })
            .addCase(getPhotoByBreed.rejected, (state, action) => {
                state.getPhotoByBreed.status = 'failed';
                state.getPhotoByBreed.error = action.payload
            })
            .addCase(getPhotoBySubBreed.pending, (state, action) => {
                state.getPhotoBySubBreed.status = 'loading';
            })
            .addCase(getPhotoBySubBreed.fulfilled, (state, action) => {
                state.getPhotoBySubBreed.status = 'succeeded';
                state.getPhotoBySubBreed.data = action.payload;
            })
            .addCase(getPhotoBySubBreed.rejected, (state, action) => {
                state.getPhotoBySubBreed.status = 'failed';
                state.getPhotoBySubBreed.error = action.payload
            })
    },
});

export const {setSelectedBreed} = slice.actions;

export const selectorAllBreedsData = (state: RootState) => state.mainSlice.allBreedsList.data

export const selectorAllBreedsStatus = (state: RootState) => state.mainSlice.allBreedsList.status

export const selectorAllBreedsError = (state: RootState) => state.mainSlice.allBreedsList.error

export const selectorBreedPhotoData = (state: RootState) => state.mainSlice.getPhotoByBreed.data

export const selectorBreedPhotoStatus = (state: RootState) => state.mainSlice.getPhotoByBreed.status

export const selectorAllBreeds = (state: RootState) => {
    let array: any = [];
    const data = state.mainSlice.allBreedsList.data
    _.map(data, (breed, idx) => array.push({breed: idx, subBreed: breed}))

    return array
}

export const selectorSelectedBreed = (state: RootState) => state.mainSlice.selectedBreed

export default slice.reducer;
