import React from 'react';
import {useSelector} from "react-redux";
import {CircularProgress} from "@mui/material";

import {HeaderContent, HeaderWrapper, Title} from './style';
import {SearchInput} from "../search-input";
import {selectorAllBreedsStatus} from "../../app/slice";

export function Header() {
    const title = 'Dogs \n Inc.'
    const allBreedsStatus = useSelector(selectorAllBreedsStatus)

    return (
        <HeaderWrapper>
            <HeaderContent>
                <Title>{title}</Title>
                <SearchInput/>
                {allBreedsStatus === 'loading' ?
                    <CircularProgress color="inherit"/>
                    :
                    <div style={{ width: 53 }}/>
                }
            </HeaderContent>
        </HeaderWrapper>
    );
}
