import styled from "@emotion/styled";

export const HeaderWrapper = styled.header`
  width: 100%;
  height: 8rem;
  position: fixed;
  top: 0;
  z-index: 1;
  background: linear-gradient(135deg, rgba(255, 255, 255, 0.1), rgba(255, 255, 255, 0));
  backdrop-filter: blur(8px);
`

export const HeaderContent = styled.div`
  position: sticky;
  width: calc(100%);
  max-width: 800px;
  height: 100%;
  margin: 0 auto;
  display: flex;
  align-items: center;
  justify-content: space-between;
`

export const Title = styled.h3`
  font-size: 1.4em;
  font-weight: bold;
  color: rgb(52, 52, 52);
  white-space: pre-line;
`


