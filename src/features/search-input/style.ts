import styled from "@emotion/styled";
import {GiSniffingDog} from "react-icons/gi";

export const SearchBar = styled.div`
  width: fit-content;
  background: white;
  box-shadow: 0 0 5px #C7C7C7;
  height: 65px;
  border-radius: 100vw;
  display: flex;
  justify-content: center;
  font-size: 0.6rem;

  :hover {
    background: #F0F0F0;
  }
`

export const SearchContent = styled.div`
  width: 100%;
  border-radius: inherit;
  padding: 0.8rem 1.5rem;
  transition: background 250ms ease;
  position: relative;
  cursor: pointer;
`

export const Input = styled.input`
  background: none;
  border: none;
  padding: 0.2rem 0 0 0;

  :focus {
    outline: none;
  }

  ::placeholder {
    font-size: 1rem;
  }
`

export const Icon = styled(GiSniffingDog)`
  position: absolute;
  top: 50%;
  right: 10px;
  transform: translateY(-50%);
  background: #b3b3b3;
  color: white;
  font-size: 1.6rem;
  padding: 0.4rem;
  border-radius: 50%;

  :hover {
    background: #de4242;
  }
`

export const AutoCompleteList = styled.ul`
  list-style: none;
  width: inherit;
  z-index: 1;
  padding: 0;
  margin: 0;
  top: 75px;
  left: 0;
  position: absolute;
  background-color: #fff;
  overflow: auto;
  max-height: 200px;
  height: auto;
  box-shadow: 0 0 5px #c7c7c7;
  border-radius: 15px;
`

export const AutoCompleteItem = styled.li`
  padding: 3px 5px;
  font-size: 1.6em;
  
  :hover {
    cursor: pointer;
    background: #F0F0F0;
  }
`
