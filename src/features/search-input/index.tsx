import {FC} from 'react';
import useAutocomplete from '@mui/material/useAutocomplete';
import {useSelector} from "react-redux";
import _ from 'lodash'

import {Props} from './props';
import {AutoCompleteItem, AutoCompleteList, Icon, Input, SearchBar, SearchContent} from './style';
import {selectorAllBreeds, setSelectedBreed} from "../../app/slice";
import {useAppDispatch} from "../../app/hooks";

export const SearchInput: FC<Props> = ({...rest}: Props) => {
    const dispatch = useAppDispatch()
    const allDogs = useSelector(selectorAllBreeds)
    const breeds = _.map(allDogs, (item) => item?.breed)
    const {
        getInputProps,
        getListboxProps,
        getOptionProps,
        groupedOptions,
    } = useAutocomplete({
        id: 'use-autocomplete',
        options: breeds,
        getOptionLabel: (option: any) => option,
        onInputChange: (e: any, value: any) => dispatch(setSelectedBreed(value)),
    });

    return (
        <SearchBar {...rest}>
            <SearchContent>
                <p style={{margin: 0, fontSize: '0.75rem'}}>Breeds</p>
                <Input {...getInputProps()} type="text" placeholder="Search breed"/>
                {groupedOptions?.length > 0 ? (
                    <AutoCompleteList onSubmit={(e: any) => console.log(e?.target)} {...getListboxProps()}>
                        {(groupedOptions as typeof allDogs)?.map((option: any, index: any) => (
                            <AutoCompleteItem key={option} value={option} {...getOptionProps({
                                option,
                                index
                            })}>{option}</AutoCompleteItem>
                        ))}
                    </AutoCompleteList>
                ) : null}
                <Icon/>
            </SearchContent>
        </SearchBar>
    );
};
