import styled from "@emotion/styled";

interface CarWrapperProps {
    readonly isActive?: boolean
}

export const CardWrapper = styled.div<CarWrapperProps>`
  display: flex;
  flex-direction: column;
  width: 200px;
  background-color: #EEEEEE;
  padding: 10px;
  border-radius: 10px;
  cursor: pointer;
  transition: transform 100ms ease-in-out, width 300ms ease-in-out;
  box-shadow: 0 0 5px #c7c7c7;


  :hover {
    box-shadow: 0 32px 32px -4px rgba(#000000, .7);
    transform: ${({isActive}) => !isActive && 'scale(1.07)'};
  }
`

export const SubBreedWrapper = styled.div`
  width: inherit;
  display: flex;
  gap: 15px;
  overflow: auto;
  margin: 10px 0;

  ::-webkit-scrollbar {
    display: none;
  }
`

export const SubBreedItem = styled.div`
  width: fit-content;
  border-radius: 10px;
  padding: 5px;
  color: #ffff;
  background-color: darkgray;
`

export const ImagesWrapper = styled.div`
  height: auto;
  width: 100%;
  display: flex;
  flex-direction: column;
`

export const ImagesContent = styled.div`
  display: flex;
  gap: 10px;
  padding: 10px;
  justify-content: space-between;
`

export const Image = styled.img`
    width: 250px;
  height: 200px;
  object-fit: fill;
  border-radius: 30px;
`


