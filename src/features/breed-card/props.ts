import { HTMLAttributes } from 'react';

export type Props = HTMLAttributes<HTMLDivElement> & {
    readonly breed: string;
    readonly subBreed: Array<string>;
    readonly isActive?: boolean;
    readonly isLoading?: boolean;
    readonly handleCardClick: (breed: string) => void;
    readonly handleUpdate: (value: any) => void;
    readonly handleCancel: (value: any) => void;
    readonly images: Array<string>;
};
