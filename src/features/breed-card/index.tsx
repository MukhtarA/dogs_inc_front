import React, {FC, useEffect, useMemo, useRef} from 'react';
import {CircularProgress, Button} from "@mui/material";
import _ from 'lodash'

import {Props} from './props';
import {CardWrapper, Image, ImagesContent, ImagesWrapper, SubBreedItem, SubBreedWrapper} from "./style";

export const BreedCard: FC<Props> = ({
                                         breed,
                                         subBreed,
                                         isLoading,
                                         isActive,
                                         handleCardClick,
                                         handleCancel,
                                         handleUpdate,
                                         images,
                                         ...rest
                                     }: Props) => {
    const randomColor = Math.floor(Math.random() * 16777215).toString(16);

    let controller = useMemo(() => new AbortController(), [images, handleCancel, handleUpdate]);

    useEffect(() => {
        let cont = new AbortController()
        if (isActive) {
            handleUpdate(cont)
        }
    }, [isActive])

    return (
        <CardWrapper
            isActive={isActive}
            onClick={() => handleCardClick(breed)}
            style={isActive ? {
                width: '100%',
                gridColumnStart: 1,
                gridColumnEnd: 4,
                backgroundColor: `#${randomColor}`
            } : {backgroundColor: `#${randomColor}`}}
            {...rest}
        >
            <h2 style={{margin: 0}}>{breed}</h2>
            {subBreed.length > 0 ? <h4 style={{margin: 0}}>Sub Breeds:</h4> : null}
            <SubBreedWrapper>
                {_.map(subBreed, (item, inx) => <SubBreedItem key={item}>{item}</SubBreedItem>)}
            </SubBreedWrapper>

            {isActive &&
            <ImagesWrapper>
                {isLoading ? <div style={{
                        width: '100%',
                        height: 220,
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}><CircularProgress color="inherit"/></div>
                    :
                    <ImagesContent>
                        {_.map(images, (item) => <Image key={item} alt={item} src={item}/>)}
                    </ImagesContent>
                }
                <Button
                    variant="contained"
                    style={{ width: 200, margin: 20, alignSelf: 'center' }}
                    color={isLoading ? 'error' : 'success'}
                    onClick={() => isLoading ? handleCancel(controller) : handleUpdate(controller)}>
                    {isLoading ? 'Click to cancel' : 'Click to update'}
                </Button>

            </ImagesWrapper>
            }
        </CardWrapper>
    );
};
