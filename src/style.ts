import styled from "@emotion/styled";

export const Grid = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  gap: 10px;
`

export const ContentWrapper = styled.div`
  width: calc(100%);
  max-width: 800px;
  height: 100%;
  margin: 0 auto;
`

export const ImagesWrapper = styled.div`
    
`
